import { Group } from './group';

export const GROUPS: Group[] = [
  { id: '11', name: 'Mr. Nice', score : 10 },
  { id: '12', name: 'Narco', score : 25 },
  { id: '13', name: 'Bombasto', score : 9 },
  { id: '14', name: 'Celeritas', score : 6 },
  { id: '15', name: 'Magneta', score : 2 },
  { id: '16', name: 'RubberMan', score : 15 },
  { id: '17', name: 'Dynama', score : 8 },
  { id: '18', name: 'Dr IQ', score : 50 },
  { id: '19', name: 'Magma', score : 30 },
  { id: '20', name: 'Tornado', score : 20 }
];