export class Group {
  id: string;
  name: string;
  score: number;
}