import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Group } from './group';
import { BuzzService } from './buzz.service';


@Component({
    selector: 'test-challenge',
    templateUrl: './test-challenge.component.html',
    styleUrls: ['./test-challenge.component.css'],
    providers: [BuzzService]
})

export class TestChallengeComponent implements OnInit {
    responsesBuzz: Group[];
    selectedResponse: Group;
    addscore: number;
    @Output() onUpdate = new EventEmitter<void>();

    constructor(private buzzService: BuzzService) { }

    getBuzzes(): void {
        this.buzzService.getBuzzes()
            .then(x => this.responsesBuzz = x);
    }

    ngOnInit(): void {
        //this.getBuzzes();  
        this.responsesBuzz = new Array<Group>();      
        var socket = new WebSocket("ws://localhost:60899/ws");
        socket.onmessage = message => {this.responsesBuzz.push(JSON.parse(message.data))};
            
      
    }




    delete(): void {
        this.buzzService.delete().then(
            () => this.responsesBuzz = new Array<Group>());

    }

    onUpdateBuzz(): void { this.onUpdate.emit(); }


}
