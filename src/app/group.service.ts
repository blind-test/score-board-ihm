import { Injectable } from '@angular/core';
import { Group } from './group';
import { ResponseBuzz } from './buzz-response';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class GroupService {

  private url = 'http://localhost:60899/api';
  constructor(private http: Http) {
   }


  getGroups(): Promise<Group[]> {
    return this.http.get(this.url + "/group")
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);


  }
  getGroup(id: string): Promise<Group> {
    return this.http.get(this.url + "/group/" + id)
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  updateGroup(id: string, addScore: number ): Promise<Group> {
   
    return this.http.put(this.url + "/group/" + id, {addScore} , this.getOptions())
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error)
  }

    private getOptions(): RequestOptions {
     return new RequestOptions({ headers: new Headers([{ "Content-Type": 'application/json' }]) });
  }

}