import { Component, OnInit, Input, OnChanges, SimpleChange } from '@angular/core';

import { Group } from './group';
import { GroupService } from './group.service';


@Component({
    selector: 'classement',
    templateUrl: './classement.component.html',
    styleUrls: ['./classement.component.css'],
    providers: [GroupService]
})

export class ClassementComponent implements OnInit, OnChanges {
    title = 'Classement';
    groups: Group[];
    selectedGroup: Group;
    @Input() updateBool :boolean;

    constructor(private groupService: GroupService) { }

    getGroups(): void {
        this.groupService.getGroups()
            .then(x => this.groups = x);
    }

    ngOnInit(): void {
        this.getGroups();
    }



ngOnChanges(changes: {[propKey: string]: SimpleChange}){
    for (let propName in changes) {
         this.getGroups();
    }
}


    onSelect(group: Group): void {
        this.selectedGroup = group;
    }

}