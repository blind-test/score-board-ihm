import { Injectable } from '@angular/core';
import { Group } from './group';
import { GroupService } from './group.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class BuzzService {

  private url = 'http://localhost:60899/api';
  constructor(private http: Http) { }


  getBuzzes(): Promise<Group[]> {
    return this.http.get(this.url + "/test")
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);


  }

  delete():  Promise<Group[]> {
    return this.http.delete(this.url + "/test").toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }




  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error)
  }



}