import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterializeModule } from 'angular2-materialize';

import { AppComponent } from './app.component';
import { GroupDetailsComponent } from './group-details.component';
import { ClassementComponent } from './classement.component';
import{TestChallengeComponent} from './test-challenge.component';
import{BuzzComponent} from './buzz.component';

@NgModule({
   imports: [
    BrowserModule,
    HttpModule,
    MaterializeModule,
    FormsModule // <-- import the FormsModule before binding with [(ngModel)]
  ],
  declarations: [
    AppComponent,
    ClassementComponent,
    GroupDetailsComponent,
    TestChallengeComponent,
    BuzzComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
