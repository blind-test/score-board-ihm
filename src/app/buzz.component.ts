import { Component, Output, EventEmitter, Input } from '@angular/core';
import { Group } from './group';
import { GroupService } from './group.service';



@Component({
    selector: 'buzz',
    templateUrl: './buzz.component.html',
    styleUrls: ['./buzz.component.css'],
    providers: [GroupService]
})

export class BuzzComponent {
    @Input() group: Group;
    hasSinger: boolean; // checkbox
    hasTitle: boolean; // checkbox
    hasBonus: boolean; // checkbox
    addscore: number;
    responseOk: boolean;
    responseKo : boolean;
    @Output() onUpdateBuzz = new EventEmitter<void>();

    constructor(private groupService: GroupService) {
        this.responseOk = false;
        this.responseKo = false;
    }


    validateResponse(): void {

        this.addscore = 0;
        if (this.hasTitle) {
            this.addscore = this.addscore + 1;
        }
        if (this.hasSinger) {
            this.addscore = this.addscore + 1;
        }
        if (this.hasBonus) {
            this.addscore = this.addscore + 1;
        }
        this.groupService.updateGroup(this.group.id, this.addscore)
            .then((group) => {
                this.onUpdateBuzz.emit();
                this.responseOk = true;
            })
            .catch((exception) => this.responseKo = true);
    }


}