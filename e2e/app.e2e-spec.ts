import { BlindTestIhmPage } from './app.po';

describe('blind-test-ihm App', function() {
  let page: BlindTestIhmPage;

  beforeEach(() => {
    page = new BlindTestIhmPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
